# MongoDB Terminology

**1. MongoDB**
MongoDB stores data in flexible, JSON-like documents, meaning fields can vary from document to document and data structure can be changed over time. NO REALATIONAL


**2. ORM**
Object Relational Mapping 
Un ORM te permite convertir los datos de tus objectos en un formato correcto para poder guardar la información en una base de datos (mapeo) creándose una base de datos virtual donde los datos que se encuentran en nuestra aplicación, quedan vinculados a la base de datos (persistencia).
Traduce y arma la secuencia sql en sentencias muy sencillas entre la base de datos y el codigo, es un intermediario.

**3. Mongoose**
Mongoose is an Object Data/Document Modeling (ODM) library for MongoDB and Node.js. It manages relationships between data, provides schema validation, and is used to translate between objects in code and the representation of those objects in MongoDB.


**4. Collections**
A collection is the equivalent of an RDBMS table. A collection exists within a single database. Collections do not enforce a schema. Documents within a collection can have different fields.
NOTE: RDBMS
Relational Database Management System. A database management system based on the relational model, typically using SQL as the query language.


**5. Documents**
MongoDB stores data records as BSON documents. BSON is a binary representation of JSON documents, though it contains more data types than JSON.

MongoDB documents are composed of field-and-value pairs and have the following structure:
{
   field1: value1,
   field2: value2,
   field3: value3,
   ...
   fieldN: value
}

![Example](https://docs.mongodb.com/manual/_images/crud-annotated-document.bakedsvg.svg)


**6. Fields**
The value of a field can be any of the BSON data types, including other documents, arrays, and arrays of documents.
For example:

var mydoc = {
               _id: ObjectId("5099803df3f4948bd2f98391"),
               name: { first: "Alan", last: "Turing" },
               birth: new Date('Jun 23, 1912'),
               death: new Date('Jun 07, 1954'),
               contribs: [ "Turing machine", "Turing test", "Turingery" ],
               views : NumberLong(1250000)
            }
ATRIBUTOS OR FIELDS

**7. Schema** 
Everything in Mongoose starts with a Schema. Each schema maps to a MongoDB collection and defines the shape of the documents within that collection.
The structure of the Document.

**8. Models**
Models are fancy constructors compiled from Schema definitions. An instance of a model is called a document. Models are responsible for creating and reading documents from the underlying MongoDB database.
INSTANCE CONSTRUCTURE MODEL AND DEPENCE OF THE SCHEMA


**9. Mongoose model (three parts)**
Requerir moongoose
Defining and creating models
Creating a model

**10. Define user model schema**

Everything in Mongoose starts with a schema. Each schema maps to a MongoDB collection and defines the shape of the documents within that collection.

To add necessary code for creating `User.js` model:

```
//require mongoose
const mongoose = require('mongoose');


//define your schema
let personSchema = new mongoose.Schema({ //with or without new
    name: { type: String, default: 'anonymous' },
    age: { type: Number, min: 18, index: true },
    bio: { type: String, match: /[a-zA-Z ]/ },
    date: { type: Date, default: Date.now },
});


//create/export a model
//modelo singular(Person with capital at firts)
module.exports  = mongoose.model('Person', personSchema);

```


**11. Query Building**

Mongoose has a very rich API that handles many complex operations supported by MongoDB. 

Create an example for getting the following in `UserModel`:

```
//THE WHOLE CODE
```
```
User.find()
    .skip(20)
    .limit(10)
    .sort({firstName: 1})
    .select({firstName: true})
    .exec()
    .then(user => res. status(200).json({result: users}))
    .catch(error => res.status(404).json({result: error}))
```

```
// find all users

User.find()

// skip the first 100 items

User.skip(20)

// limit to 10 items

User.limit(10)

// sort ascending by firstName

.sort({firstName: 1})

// select firstName only

.select({firstName: true})

// execute the query
.exec()

```


